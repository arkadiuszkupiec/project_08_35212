#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <map>
#include <sstream>
#include <Windows.h>
#include <limits>
#include <iomanip>

using namespace std;


// Slowniki zawierajace "nazwa": (int)"cena"
map<string, float> PizzaMenu;
map<string, float> NapojeMenu;

// ==================================================
vector<string>calaPizza;
vector<string>pizza50_50;
vector<string>napoje;
vector<string>gratis;
float suma = 0;
bool discount = false;

void finish(bool forParagon);

void paragon(bool discount)
{
    system("CLS");
    finish(true);
    if(gratis.size() > 0){
         cout << setw(20) << gratis[0]; cout << setw(9) << "gratis" << endl;
    }
    if(discount)
    {
        suma *= 0.8;
        cout << setw(20) << " ";
        cout << setw(9) << "-20%" << endl;
    }
    cout << "\n" << setw(21) << "Suma:"; printf("%6.2f",suma); cout << "zl\n" << endl;
    system("pause");
}

void showPizza(){

    // stworzenie slownika ponumerowanego od 1 w gore, gdzie klucz to liczba a wartosc to nazwa pizzy
    // =================================
    map<int, string> PizzaByNum;
    int num = 1;
    for(auto const &para: PizzaMenu){
        PizzaByNum[num] = para.first;
        num++;
    }
    // =================================

    system("CLS");
    int i=1;
    cout << "Menu: " << endl;
    for(auto const &para: PizzaMenu)
    {
        cout << i << ". " << setw(10) << para.first;
        cout << setprecision(2) << setw(5) << para.second;
        cout << "zl" << endl;
        i++;
    }
    cout << "0. " << setw(10) << "Powrot" << endl;
    int pizzachoice;

    while (std::cout << "Wybor: " && !(std::cin >> pizzachoice) || (pizzachoice< 0 || pizzachoice > PizzaMenu.size()))
    {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Nieprawidlowy wybor, wybierz opcje od 0-" << PizzaMenu.size() << endl;
        Sleep(2000);
        system("CLS");
        showPizza();
        return;
    }
    // powrot do menu ==============
    if(pizzachoice == 0) return;
    // =============================

    calaPizza.push_back(PizzaByNum[pizzachoice]);
    cout << "=====DODANO=====" << endl;
    Sleep(1000);
    showPizza();
}

void showPizza50(bool second = false) {
    // stworzenie slownika ponumerowanego od 1 w gore, gdzie klucz to liczba a wartosc to nazwa pizzy
    // =================================
    map<int, string> PizzaByNum;
    int num = 1;
    for (auto const& para : PizzaMenu) {
        PizzaByNum[num] = para.first;
        num++;
    }
    // =================================

    system("CLS");
    int i = 1;
    second != true ? cout << "Wybierz pierwsza polowke: " << endl : cout << "Wybierz druga polowke: " << endl;
    for (auto const& para : PizzaMenu)
    {
        cout << i << ". " << setw(10) << para.first;
        cout << setprecision(2) << setw(5) << para.second;
        cout << "zl" << endl;
        i++;
    }
    cout << "0. " << setw(10) << "Powrot" << endl;
    int pizzachoice;

    while (std::cout << "Wybor: " && !(std::cin >> pizzachoice) || (pizzachoice< 0 || pizzachoice > PizzaMenu.size()))
    {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Nieprawidlowy wybor, wybierz opcje od 0-" << PizzaMenu.size() << endl;
        Sleep(2000);
        system("CLS");
        showPizza();
        return;
    }
    // powrot do menu ==============
    if (pizzachoice == 0 && second != true) return;
    else if (pizzachoice == 0 && second == true)
    {
        pizza50_50.pop_back();
        return;
    }
    // =============================

    pizza50_50.push_back(PizzaByNum[pizzachoice]);
    cout << "=====WYBRANO POLOWKE=====" << endl;
    Sleep(1000);
    if (!second)
    {
        showPizza50(true);
    }
    else
        return;
}

void showDrinks(bool isgratis = false){
    // stworzenie slownika ponumerowanego od 1 w gore, gdzie klucz to liczba a wartosc to nazwa napoju
    // =================================
    map<int, string> DrinkByNum;
    int num = 1;
    for(auto const &para: NapojeMenu){
        DrinkByNum[num] = para.first;
        num++;
    }
    // =================================

    system("CLS");
    int i=1;
    if(!isgratis)
    {
        cout << "Menu: " << endl;
        for(auto const &para: NapojeMenu)
        {
            cout << i << ". " << setw(10) << para.first;
            cout << setprecision(2) << setw(5) << para.second;
            cout << "zl" << endl;
            i++;
        }
        cout << "0. " << setw(10) << "Powrot" << endl;
    }
    else
    {
        cout << "Wybierz napoj gratisowy: " << endl;
        for(auto const &para: NapojeMenu)
        {
            cout << i << ". " << setw(10) << para.first << endl;
            i++;
        }
        cout << "0. " << setw(10) << "Powrot" << endl;
    }

    int drinkchoice;

    while (std::cout << "Wybor: " && !(std::cin >> drinkchoice) || (drinkchoice< 0 || drinkchoice > PizzaMenu.size()))
    {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Nieprawidlowy wybor, wybierz opcje od 0-" << NapojeMenu.size() << endl;
        Sleep(2000);
        system("CLS");
        showDrinks();
        return;
    }
    // powrot do menu ==============
    if(drinkchoice == 0) return;
    // =============================

    if(!isgratis)
    {
        napoje.push_back(DrinkByNum[drinkchoice]);
        cout << "=====DODANO=====" << endl;
        Sleep(1000);
        showDrinks();
    }
    else
    {
        gratis.push_back(DrinkByNum[drinkchoice]);
        cout << "=====WYBRANO=====" << endl;
        Sleep(1000);
        return;
    }
}

void finish(bool forParagon = false) {
    int liczba_pizz = 0;
    suma = 0;
    if (!forParagon)
    {
        liczba_pizz = 0;
        discount = false;
    }

    if (calaPizza.size() > 0)
    {
        for (auto& pizza : calaPizza)
        {
            float cena = PizzaMenu[pizza];
            cout << setw(20) << pizza;
            printf("%7.2f", cena);
            cout << "zl" << endl;
            suma += cena;
            liczba_pizz++;
        }
    }
    // ------------------------------------------
    if (pizza50_50.size() > 0)
    {
        float cena;
        for (int i = 0; i <= pizza50_50.size() / 2; i += 2)
        {
            cena = (PizzaMenu[pizza50_50[i]] + PizzaMenu[pizza50_50[i + 1]]) / 2;
            suma += cena;
            string pizzaJoint = pizza50_50[i] + "/" + pizza50_50[i + 1];
            cout << setw(20) << pizzaJoint;
            printf("%7.2fzl\n", cena);
            liczba_pizz++;
        }
    }
    // ------------------------------------------
    if (napoje.size() > 0)
    {
        float cena;
        for (auto& napoj : napoje)
        {
            cena = NapojeMenu[napoj];
            cout << setw(20) << napoj;
            printf("%7.2f", cena);
            cout << "zl" << endl;
            suma += cena;
        }
        // ------------------------------------------
    }
    // jesli wywolano ta funkcje poprzez funckje 'Paragon' - to wyswietla sie tylko zakupione rzeczy
    if (forParagon) return;

    if (suma <= 0)
    {
        cout << "====KOSZYK JEST PUSTY====" << endl;
        Sleep(1500);
        return;
    }
    if (suma >= 100)
    {
        discount = true;
        suma *= 0.8; // warunek obnizki
        cout << "=====Aktywowano znizke 20%=====" << endl;
    }

    cout << "\n" << setw(21) << "Suma:"; cout; printf("%6.2f", suma); cout << "zl\n" << endl;
    cout << "1. Zloz zamowienie" << endl;
    cout << "2. Anuluj zamowineie" << endl;
    cout << "0. Powrot" << endl;
    int choice;

    while (std::cout << "Wybor: " && !(std::cin >> choice) || (choice < 0 || choice > 2))
    {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Nieprawidlowy wybor, wybierz opcje od 0-2 \n\n";
        Sleep(2000);
        system("CLS");
        finish();
    }

    if (choice == 0) return; // powrot do menu

    else if (choice == 2)
    {
        suma = 0;
        napoje.clear();
        calaPizza.clear();
        pizza50_50.clear();
        gratis.clear();
        cout << "=====ANULOWANO=====" << endl;
        Sleep(2000);
        return;
    }
    else if (liczba_pizz >= 2)
    {
        showDrinks(true);
        system("CLS");
    }
    cout << "=====ZLOZONO ZAMOWIENIE======" << endl;
    paragon(discount);
    suma = 0;
    napoje.clear();
    calaPizza.clear();
    pizza50_50.clear();
    gratis.clear();
    return;
}

void displayMenu()
  {
    cout << "Wybierz opcje: " << endl;
    cout << "1. Zamow cala pizze " << endl;
    cout << "2. Zamow pizze 50 na 50 " << endl;
    cout << "3. Zamow napoj" << endl;
    cout << "4. Do koszyka " << endl;
    cout << "0. Przerwij" << endl;
  }

void wczytajMenu(string filepath, map<string,float> &dict)
{
    fstream fin;
    fin.open(filepath, ios::in);

    string line, word, temp;

    // dodanie do temp calej linii
    while (getline(fin, temp)) {
    // ----------------------
        // cout << temp << endl;   // Wydrukowanie calej lini

        stringstream ss(temp);
        string key;
        int value;

        // dodawanie kolejnych elemntow do slownika, ktory jest parametrem
        int i = 0;
        while(getline(ss, word, ';'))
        {
            if(i == 0)
            {
                key = word;
                i++;
            }
            else if(i == 1)
            {
                value = atoi(word.c_str());
                i++;
            }
            else
            {
                dict[key] = value;
                i = 0;
            }
        }
        dict[key] = value;
    }
    fin.close();
}

int main()
{
    // ==================================================
    // Wywolanie funkcji, ktora pobiera nazwe pliku.txt i slownik do zapisania wartosci
    wczytajMenu("pizza_menu.txt", PizzaMenu);
    /*
    for (auto const& x : PizzaMenu)
    {
        std::cout << x.first  // string (key)
                  << ": "
                  << x.second // string's value
                  << std::endl;
    }
    cout << endl;
    */
    wczytajMenu("napoje_menu.txt", NapojeMenu);
    /*
    for (auto const& x : NapojeMenu)
    {
        std::cout << x.first  // string (key)
                  << ": "
                  << x.second // string's value
                  << std::endl;
    }

    */
    int choice;
    bool run = TRUE;
    while(run)
    {
        system("CLS");
        displayMenu();

        // zabezpieczenie przed nieprawidlowym wyborem (zarowno typ jak i zakres numeryczny)
        while (std::cout << "Wybor: " && !(std::cin >> choice) || (choice < 0 || choice > 4))
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Nieprawidlowy wybor, wybierz opcje od 0-4 \n\n";
            Sleep(2000);
            system("CLS");
            displayMenu();
        }
        system("CLS");

        switch (choice)
        {
            case 1:
                showPizza();
                break;
            case 2:
                showPizza50();
                break;
            case 3:
                showDrinks();
                break;
            case 4:
                finish();
                break;
            case 0:
                run = FALSE;
        }
    }

    return 0;
}
